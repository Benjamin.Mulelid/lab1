package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        Boolean isPlaying = true;

        while (isPlaying) {
            System.out.println("Let's play round " + roundCounter);

            String humanMove = getHumanMove();

            // Generate a random computer move
            Random random = new Random();
            String computerMove = rpsChoices.get(random.nextInt(3));
    
            // Get winner
            if (getWinner(humanMove, computerMove)) {
                System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". Human wins!");
                humanScore++;
            }
            else if (getWinner(computerMove, humanMove)) {
                System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". Computer wins!");
                computerScore++;
            }
            else {
                System.out.println("Human chose " + humanMove + ", computer chose " + computerMove + ". It's a tie!");
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            isPlaying = continuePlaying();
            roundCounter++;
        }

        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    /**
     * Gets the human's move and checks whether the input is correct
     * @return the human's move as a string
     */
    public String getHumanMove() {
        while (true) {
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();

            if (humanMove.equals("rock") || humanMove.equals("paper") || humanMove.equals("scissors")) {
                return humanMove;
            }
            else {
                System.out.println("I do not understand " + humanMove + ". Could you try again?");
            }
        }
    }

    /**
     * Takes two human moves as input and returns the winner
     * @param choice1
     * @param choice2
     * @return true if the first human won, false if not
     */
    public Boolean getWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        } 
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }    
        else {
            return choice2.equals("scissors");
        }
    }

    /**
     * Ask the human if they want to continue playing
     * @return true if the human wants to continue, false if not
     */
    public Boolean continuePlaying() {
        while (true) {
            String input = readInput("Do you wish to continue playing? (y/n)?").toLowerCase().trim();
        
            if (input.equals("y")) {
                return true;
            }
            else if (input.equals("n")) {
                return false;
            }
            else {
                System.out.println("I do not understand " + input + ". Could you try again?");
            }
        }
    }

}
